import React from 'react';
import { connect } from 'react-redux';


class SongDetail extends React.Component {
    render() {
        if (!this.props.song) {
            return <div>Select a song from the list</div>;
        }

        let { title, duration } = this.props.song;

        return (
            <div>{title}</div>
        )
    }
}

const mapStateToProps = (state) => ({
    song: state.selectedSong
});

export default connect(mapStateToProps)(SongDetail);