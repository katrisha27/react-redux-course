import React from 'react';

class ImageCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = { spans: 0 };
        this.imageRef = React.createRef();
    }

    componentDidMount() {
        // image is not loaded yet - so clientHeight will be 0 in console
        console.log(this.imageRef.current.clientHeight);

        this.imageRef.current.addEventListener('load', this.setSpans);
    }

    setSpans = () => {
        let height = this.imageRef.current.clientHeight;
        let spans = Math.ceil(height / 10);

        this.setState({ spans });
    };

    render() {
        let { description, urls } = this.props.image;

        return (
            <div style={{ gridRowEnd: `span ${this.state.spans}`}}>
                <img alt={description} src={urls.regular} ref={this.imageRef} />
            </div>
        )
    }
}

export default ImageCard;