import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID 0e90d6a155cb0b72f494a406b064e8475dba1059fbc0cabcfae6e734fd7d7073'
    }
});