import React from 'react';

class SeasonDisplay extends React.Component {

    seasonConfig = {
        summer: {
            text: 'Lets hit the beach',
            iconName: 'sun'
        },
        winter: {
            text: 'Burr, it is chilly',
            iconName: 'snowflake'
        }
    };

    render() {
        let season = this.getSeason(this.props.latitude, new Date().getMonth());
        let { text, iconName } = this.seasonConfig[season];

        return (
            <div>
                <i className={`massive ${iconName} icon`} />
                <h1>{text}</h1>
                <i className={`massive ${iconName} icon`} />
            </div>
        )
    }

    getSeason(latitude, month) {
        return (month > 2 && month < 9)
            ? latitude > 0 ? 'summer' : 'winter'
            : latitude > 0 ? 'winter' : 'summer';
    }
}

export default SeasonDisplay;