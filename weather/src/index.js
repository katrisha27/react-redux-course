import React from 'react';
import ReactDOM from 'react-dom';
import Spinner from './Spinner';
import SeasonDisplay from './SeasonDisplay';

class App extends React.Component {

    state = { latitude: null, errorMessage: '' };

    render () {
        return (
            <div>
                { !this.state.latitude && !this.state.errorMessage &&
                    <Spinner />
                }
                { this.state.latitude &&
                    <SeasonDisplay latitude={this.state.latitude} />
                }
                { this.state.errorMessage &&
                    <div>Error: {this.state.errorMessage}</div>
                }
            </div>
        )
    } 

    componentDidMount() {
        this.setUserLatitude();
    }

    setUserLatitude() {
        window.navigator.geolocation.getCurrentPosition(
            (position) => this.setState({ latitude: position.coords.latitude}),
            (error) => this.setState({ errorMessage: error.message })
        );
    }
}

ReactDOM.render(<App/>, document.querySelector('#root'));