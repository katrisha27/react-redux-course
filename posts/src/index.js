import React from 'react';
import ReactDOM from 'react-dom';
import ApprovalCard from './ApprovalCard';
import CommentDetail from './CommentDetail';
import faker from 'faker';

const App = () => (
    <div className="ui container comments">
        <ApprovalCard>
            <div>
                <h4>Warning!</h4>
                Are you you want to do this?</div>
        </ApprovalCard>
        <ApprovalCard>
            <CommentDetail
                author="Kate"
                comment="VBlalmfsd dsfdsfds"
                avatar={faker.image.avatar()}
            />
        </ApprovalCard>

        <CommentDetail
            author="Elena"
            comment="VBlalmfsd dfsdfsdfsdf dsfdsfds"
            avatar={faker.image.avatar()}
        />
    </div>
);

ReactDOM.render(
   <App />,
    document.getElementById('root')
);