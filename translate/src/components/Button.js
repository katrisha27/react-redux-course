import React from 'react';
import ColorContext from '../contexts/ColorContext';
import LanguageContext from '../contexts/LanguageContext';

class Button extends React.Component {
    renderSubmit(lang) {
        return lang === 'en' ? 'Submit' : 'Отправить';
    }

    renderButton(color) {
        return (
            <button className={`ui button ${color === 'red' ? 'red' : 'primary'}`}>
                <LanguageContext.Consumer>
                    {({ lang }) => this.renderSubmit(lang)}
                </LanguageContext.Consumer>
            </button>
        )
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(color) => this.renderButton(color)}
            </ColorContext.Consumer>
        )
    }
}

export default Button;