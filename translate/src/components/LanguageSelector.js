import React from 'react';
import LanguageContext from '../contexts/LanguageContext';

class LanguageSelector extends React.Component {
    static contextType = LanguageContext;

    render() {
        return (
            <div>
                Select language:
                <i className="flag us" onClick={() => this.context.onLanguageChange('en')}></i>
                <i className="flag ru" onClick={() => this.context.onLanguageChange('ru')}></i>
            </div>
        )
    }
}

export default LanguageSelector;