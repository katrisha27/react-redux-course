import React from 'react';
import { connect } from 'react-redux';
import { fetchPostsAndUsers } from '../actions';
import UserHeader from './UserHeader';

class PostList extends React.Component {
    render() {
        return (
            <div>{this.renderList()}</div>
        )
    }

    componentDidMount() {
        /*this.props.fetchPosts();*/
        this.props.fetchPostsAndUsers();
    }

    renderList() {
        return this.props.posts.map((post) => {
            return (
                <div key={post.id}><span>{post.title}</span><h3><UserHeader userId={post.userId} /></h3></div>
            )
        })
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.posts
    }
};

export default connect(mapStateToProps, { fetchPostsAndUsers })(PostList);