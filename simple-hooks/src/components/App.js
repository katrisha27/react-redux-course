import React, { useState } from 'react';
import UserList from './UserList';
import ResourceList from './ResourceList';

const App = () => {
    /* useState(initialValue):
        1 - current value (resource): value of the piece of state (like this.state.resource);
        2 - setCurrentValue (setSource): function to call to upgrade our state.
    */
    const [resource, setSource] = useState('posts');

    return (
        <div>
            <UserList />
            <div>
                <button onClick={() => setSource('posts')}>Posts</button>
                <button onClick={() => setSource('todos')}>Todos</button>
            </div>
            <ResourceList resource={resource} />
        </div>
    );
};

export default App;