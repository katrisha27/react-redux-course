import React, { useState, useEffect } from 'react';
import axios from 'axios';

const useResources = (resource) => {
    const [resources, setResources] = useState([]);

    const fetchResource = async resource => {
        try {
            const response = await axios.get(`https://jsonplaceholder.typicode.com/${resource}`);

            setResources(response.data);
        } catch (error) {
            console.log('Something wrong with requested url. Error is: ' + error.response.statusText);
        }

    };

    /* useEffect:
     1 - function is called when second param is updated!
     2 - value (it is compared with previous one and call first function is differs):
     -- if NO - leads to non-stop renders just like without comparison in componentDidUpdate (ALWAYS)
     -- [] - just like componentDidMount (just ONE time)
     -- [resource] - componentDidMount + componentDidUpdate with comparison (ALWAYS if state differs)
     */
    useEffect(
        () => {
            fetchResource(resource);
        },
        [resource]
    );

    return resources;
};

export default useResources;